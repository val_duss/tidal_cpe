<?php

include_once 'input.php';

class Password extends Inputform 
{
	private $name;
	private $id;
	private $label;
	
	function __construct($name, $id = "",$label="")
	{
		$this->name = $name;
		if($id == ""){
			$id = $name;
		}
		$this->id = $id;
		$this->label = $label;
	}

	function getName(){
		return $this->name;
	}

	function affiche(){
		if ($this->label != ""){
			echo "<label for='".$this->name."'>".$this->label."</label>";
		}
		echo "<input type='password' id='".$this->id."' name='".$this->name."' required><br><br>";
	}

}