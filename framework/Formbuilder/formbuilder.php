<?php

include_once 'input.php';
include_once 'textfield.php';
include_once 'password.php';
include_once 'hidden.php';


class FormBuilder 
{

	private $title;
	private $inputs;
	private $redirect;
	private $method;
	private $submit;
	
	function __construct($title, $redirect="")
	{
		$this->title = $title;
		$this->inputs = array();
		$this->redirect = $redirect;
		$this->method = "post";
		$this->submit = "submit";
	}

	/***
	 * Set de la methode POST
	*/
	function setMethodePost(){
		$this->method = "post";
	}

	/***
	 * Set de la methode GET
	*/
	function setMethodeGet(){
		$this->method = "get";
	}

	/**
	 * Set de l'url de redirection
	*/
	function setRedirectUrl($redirect){
		$this->redirect = $redirect;
	}

	/**
	 * Ajout d'un nouveau TextField
	*/
	function addNewTextField($name, $id="", $label=""){
		array_push($this->inputs, new TextField($name, $id, $label));
	}

	/**
	 * Ajout d'un nouveau Password
	*/
	function addNewPassword($name, $id="", $label=""){
		array_push($this->inputs, new Password($name, $id, $label));
	}

	/**
	 * Ajout d'un nouveau Hidden
	*/
	function addNewHidden($name, $value){
		array_push($this->inputs, new Hidden($name, $value));
	}

	function setSubmit($value){
		$this->submit = $value;
	}

	/**
	 * Affichage du FrameWork
	*/
	function affiche(){
		echo "<form action='".$this->redirect."' method='".$this->method."'>";
		foreach ($this->inputs as $input) {
			$input->affiche();
		}
		echo "<input type='submit' value='".$this->submit."'>";
		echo "</form>";
	}
}