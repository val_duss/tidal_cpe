<?php

include_once 'input.php';

class Hidden extends Inputform 
{
	private $name;
	private $value;
	
	function __construct($name, $value)
	{
		$this->name = $name;
		$this->value = $value;
	}

	function getName(){
		return $this->name;
	}

	function getValue(){
		return $this->value;
	}

	function affiche(){
		echo "<input id='".$this->name."' name='".$this->name."' type='hidden' value='".$this->value."'>";
	}

}