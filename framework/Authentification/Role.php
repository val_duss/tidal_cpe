<?php

/**
 * Class Role
 * Objet Role
 * 
 */
class Role {

    public $label; //Nom du role
    public $pages; //Tableau des pages whitelist

    function __construct($label) {
        $this->label = $label;
        $this->pages = array();
    }

    /**
     * Function deletePage
     * Enlève une page de la liste des page whiteliste */
    function deletePage($page) {
        unset($this->pages[$page]);
    }

    /**
     * Function addRulesOnePage
     * Ajoute une page dans la liste des page whitelist */
    function addRulesOnePage($page) {
        array_push($this->pages, $page);
    }

    /**
     * Function addRulesMultiPages
     * Ajoute plusieurs page dans la liste des pages whitelist */
    function addRulesMultiPages($pages) {
        foreach ($pages as $page) {
            addRulesOnePage($page);
        }
    }

    /**
     * Function isAuthorized
     * Verifie si la page demandé est whitelist
     * retourne true si oui ou false si non */
    function isAuthorized($pageRequest) {
        foreach ($this->pages as $page) {
            if ($page == $pageRequest) {
                return true;
            }
        }
        return false;
    }

}
