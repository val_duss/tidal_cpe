<?php

include_once "../framework/Framework.php";
include_once __DIR__ . "/Users.php";
include_once __DIR__ . "/Role.php";

/**
 * 
 * Class User
 * Objet Utilisateur
 */
class User {

    public $id_user; //Id de l'utilisateur
    public $username; //Nom de l'utilisateur
    private $password; //Mdp de l'utilisateur
    private $roles; //Liste des roles de l'utilisateur
    private $attributes; //Liste des attributs de l'utilisateur

    function __construct($id_user, $username, $password, $attributes) {
        $this->id_user = $id_user;
        $this->username = $username;
        $this->password = Framework::encode($password);
        $this->roles = array();
        $this->attributes = $attributes;
    }

    /**
     * Function checkPassword
     * Verifie que le mdp soit correcte
     * retourn true si vrai sinon false */
    function checkPassword($password) {
        if (Framework::encode($password) == $this->password) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * function addRole
     * Ajoute un role à l'utilisateur
     * */
    function addRole($role) {
        array_push($this->roles, $role);
    }

    /**
     * Function deleteRole
     * Enlève un role de la liste des role de l'utilisateur */
    function deleteRole($role) {
        unset($this->roles, $role);
    }

    /**
     * function haveRole
     * Verifie si l'utilisateur posède le role demandé
     * Retourn true si vrai sinon false */
    function haveRole($roleRequest) {
        foreach ($this->roles as $role) {
            if ($role == $roleRequest) {
                return true;
            }
        }
        return false;
    }

    /**
     * function isAuthorized
     * Verifie si l'utilisateur à le droit de voir la page demandé
     * retourne true si vrai sinon false */
    function isAuthorized($page) {
        foreach ($this->roles as $role) {
            if ($role->isAuthorized($page)) {
                return true;
            }
        }
        return false;
    }

    /**
     * function getAttributes 
     * Récupère la liste des attributs de l'utilisateur */
    function getAttributes() {
        return $this->attributes;
    }

    /**
     * function getAttribute
     * Recupère la valeur de l'attribut passé en paramêtre */
    function getAttribute($label) {
        foreach ($this->attributes as $attribute) {
            if ($attribute[1] == $label) {
                return $attribute;
            }
        }
        return null;
    }

    /**
     * function setAttribut
     * donne la valeur passé en paramêtre à l'utilisateur passé en paramêtre */
    function setAttribut($label, $value) {
        $i = 0;
        foreach ($this->attributes as $attribute) {
            if ($attribute[1] == $label) {
                $attribute[2] = $value;
                $this->attributes[$i] = $attribute;
            }
            $i++;
        }
    }

}
