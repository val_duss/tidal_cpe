<?php

include_once __DIR__ . "/User.php";
include_once __DIR__ . "/Session.php";
include_once __DIR__ . "/Role.php";

/**
 * Class Users
 * Liste des utilisateurs et des roles
 * 
 */
class Users {

    private $users; //tableau d'utilisateur
    private $session; //session active
    private $attributes; //Liste des attributs

    function __construct($session) {
        $this->session = $session;
        $this->users = array();
        $this->attributes = array();
    }

    /**
     * Function register
     * Permet de créer l'utilisateur dans la liste d'utilisateurs
     * */
    function register($username, $password, $attributes) {
        if ($username != null && $password != null) {
            if (count($this->attributes) == count($attributes)) {
                $tempAttributes = array();
                foreach ($this->attributes as $key => $attribute) {
                    $tempAttributes[] = [$attribute[0], $attribute[1], $attributes[$key]];
                }
                $tempUser = new User(count($this->users), $username, $password, $tempAttributes);
                $this->users[$tempUser->id_user] = $tempUser;
                return $tempUser;
            } else {
                echo "Erreur: il manque des attributs dans l'ordre:";
                foreach ($this->attributes as $attribute) {
                    echo $attribute[0];
                }
                echo "<br>";
            }
        } else {
            $erreur = "";
            $username === null ? $erreur = "nom d'utilisateur" : $erreur = "mot de passe";
            throw new Exception("Le $erreur est vide.");
        }
    }

    /**
     * Function login
     * Permet à l'utilisateur de se connecter et de créer une session
     * Retourn true si connexion réussite ou false
     */
    function login($username, $password) {
        foreach ($this->users as $user) {
            if ($user->username == $username and $user->checkPassword($password)) {
                $this->session->startSession($user->id_user);
                return true;
            }
        }
        return false;
    }

    /**
     * Function getUserById
     * Permet de récupérer un utilisateur par son id
     * retourne un objet User ou un null */
    function getUserById($id_user) {
        foreach ($this->users as $user) {
            if ($user->id_user == $id_user) {
                return $user;
            }
        }
        return null;
    }

    /**
     * Function getUsersByRole
     * Retourne tout les utilisateurs avec le même role
     * retourn un tableau de type User */
    function getUsersByRole($role) {
        $temp = array();
        foreach ($this->users as $user) {
            if ($user->haveRole($role)) {
                array_push($temp, $user);
            }
        }
        return $temp;
    }

    /**
     * Function length
     * retourne le nombre d'utilisateur
     * */
    function length() {
        return count($this->users);
    }

    /**
     * function AddAtribute
     * Création d'un nouvel attribut pour les utilisateur
     * Paramêtre Type, Label */
    function newAttribute($type, $label) {
        array_push($this->attributes, array($type, $label));
    }

    /**
     * Function deleteAttribute
     * Enlève un attribut de la liste des attribut */
    function deleteAttribute($attribute) { //TODO le faire en cascade
        unset($this->attributes, $attribute);
    }

    /**
     * function getAttributesList
     * Retourne la listes des attributs * */
    function getAttributesList() {
        return $this->attributes;
    }

    /**
     * function getSession
     * Retourne la session * */
    function getSession() {
        return $this->session;
    }

}
