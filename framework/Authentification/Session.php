<?php

/**
 * 
 * Class Session
 * Classe permettant d'établir une session
 * */
class Session {

    function __construct(){
        session_start();
    }

    /**
     * function startSession
     * Commence une session et fait passer en paramêtre l'id de l'utilisateur demandé */
    function startSession($id_user=-1) {
        $_SESSION['id_user'] = $id_user;
    }

    /**
     * function stopSession
     * Arret une session */
    function stopSession() {
        session_destroy();
        unset($_SESSION);
    }

    /**
     * function isConnected
     * Vérifie si un utilisateur est connecté 
     * Retourn true si vrai ou false sinon */
    function isConnected() {
        if (isset($_SESSION['id_user'])) {
            if ($_SESSION['id_user'] != -1) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * function getConnectedUserID
     * Récupère l'id de l'utilisateur connecté */
    function getConnectedUserID() {
        if (isset($_SESSION)) {
            return $_SESSION['id_user'];
        } else {
            return -1;
        }
    }

}
