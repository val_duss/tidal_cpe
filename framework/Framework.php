<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of framework
 *
 * @author eloi.cambray-lagassy
 */
class Framework {

    public static function readConfig($prmConfigFileName) {
        return parse_ini_file(__DIR__ . "/../config/" . $prmConfigFileName);
    }

    public static function encode($text){
        return sha1($text);
    }


    public static function listAllPHPFileFromFolder($prmFolder) {
        $filesName = scandir(__DIR__ . "/../src/" . $prmFolder);
        $phpFiles = array();
        foreach ($filesName as $name) {
            $extension = pathinfo($name, PATHINFO_EXTENSION);
            if ($extension === "php") {
                $phpFiles[] = $name;
            }
        }
        return $phpFiles;
    }

}
