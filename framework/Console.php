<?php

include_once 'ORM/ORM.php';
include_once __DIR__. '/../src/entities/Article.php';

/**
 *
 * Fichier PHP d'entrée dans le framework en mode console pour effectuer des actions en back automatiquement
 * 
 * @author eloi.cambray-lagassy
 */
$orm = new ORM();
$em = $orm->getEntityManager();

if (is_array($argv) && sizeof($argv) > 1) {
    switch ($argv[1]) {
        case "orm:synchronisation":
            echo "Synchronisation de la BDD avec les entités.\r\n";
            $orm->migrateDB();
            break;
        case "orm:import":
            echo "Création d'articles pour tester le framework via l'ORM.\r\n";
            $article1 = new Article();
            $article1->setTitre("Bienvenue dans ce premier article");
            $article1->setCorps("C'est juste du texte pour tester.");
            $article1->setPied("Par eloi...");
            $article1->setOrdre(1);
            $em->persist($article1);
            $article2 = new Article();
            $article2->setTitre("Bienvenue dans ce deuxieme article");
            $article2->setCorps("J'espère que ce contenu de qualité vous plait.");
            $article2->setPied("Par Valentin...");
            $article2->setOrdre(2);
            $em->persist($article2);
            $em->flush();
            break;
        default:
            echo "Commande inexistante, utilisez help pour lister les différentes actions possible.\r\n";
    }
} else {
    echo "Utilisez help pour lister les commandes existantes.\r\n";
}
