<?php

class Controller{

    protected $name;
    protected $model;
    protected $view;

    function __construct($name="home"){
        $this->model = "Model";
        $this->view = "View";
        $this->name = $name;
    }

    /**
     * Set du model
    */
    function setModel($model){
        $this->model = $model;
    }

    /**
     * Set de la vue
    */
    function setView($view){
        $this->view = $view;
    }

    /**
     * Get du nom
    */
    function getName(){
        return $this->name;
    }
  
    /**
     * Redirection
    */
    protected function goTo($destination){
        header('Location: ' . $destination );
    }

    /**
     * Affichage du Controlleur
    */
    function affiche(){
        require('../src/models/'.$this->model.'.php');
        $title = $this->name;
        $content = getContent();
        require('../src/views/'.$this->view.'.php');
    }

}



?>