<?php

class Routing{

    private $routes;

    function __construct($routes=array()){
        $this->routes = $routes;
    }

    /**
     * Ajout d'une route
    */
    function addRoute($controller){
        array_push($this->routes, $controller);
    }

    /**
     * Routing
    */
    function routing(){
        if(isset($_GET['page']) && !empty($_GET['page'])){
             $page = $_GET['page'];
             foreach($this->routes as $route){
                if($route->getName() == $page){
                    $route->affiche();
                }
             }
        }else{
            $ctrl = new Controller();
            $ctrl->affiche();
        }
    }
}