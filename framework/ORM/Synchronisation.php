<?php

/**
 * Synchronisation de la base de données avec la liste d'entité définie dans le
 * dossier src/entities utilisé par l'utilisateur du framework
 *
 * @author eloi.cambray-lagassy
 */
class Synchronisation {

    private $entitiesAttributes;
    private $databaseManager;

    public function __construct(DatabaseManager $databaseManager) {
        $this->databaseManager = $databaseManager;
    }

    public function migrateBdd() {
        $orm = new ORM();
        $this->databaseManager = new DatabaseManager($orm->getConn());

        $this->entitiesAttributes = [];
        $entitiesFiles = Framework::listAllPHPFileFromFolder("entities");
        foreach ($entitiesFiles as $fileName) {
            include_once __DIR__ . '/../../src/entities/' . $fileName;
            $entity = pathinfo($fileName, PATHINFO_FILENAME);
            foreach (get_class_methods($entity) as $method) {
                if (strpos($method, "get") === 0) {
                    $this->entitiesAttributes[$entity][] = strtolower(substr($method, 3));
                }
            }
        }

        if ($orm->isUsersInBase()) {
            include_once __DIR__ . '/UserEntity.php';
            foreach (get_class_methods("UserEntity") as $method) {
                if (strpos($method, "get") === 0) {
                    $this->entitiesAttributes["User"][] = strtolower(substr($method, 3));
                }
            }
        }

        foreach ($this->entitiesAttributes as $table => $attributes) {
            $this->databaseManager->createTable($table);
            foreach ($attributes as $attribute) {
                if ($attribute !== "id") {
                    $this->databaseManager->createChamp("ORM_" . $table, $attribute, "", true);
                }
            }
        }
    }

}
