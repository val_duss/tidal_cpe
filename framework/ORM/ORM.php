<?php

/*
 *  
 *  ORM du Framework du projet TIDAL
 * 
 *  - EntityManager => findAll d'une entité, Find à partir d'un ID, insertion
 *    en base et suppression
 * 
 *  - DatabaseManager => permet de gérer toute la BDD depuis le dossier d'entité
 *    du framework
 * 
 *  - Synchronisation => permet de réaliser les requetes SQL permettant de
 *    gérer la structure de la base
 * 
 */

include_once __DIR__ . "/../Framework.php";
include_once __DIR__ . "/EntityManager.php";
include_once __DIR__ . "/DatabaseManager.php";
include_once __DIR__ . "/Synchronisation.php";

/**
 *
 * @author eloi.cambray-lagassy
 */
class ORM {

    private $host;
    private $user;
    private $pass;
    private $port;
    private $dbname;
    private $extension;
    private $conn;
    private $em;
    private $dm;
    private $synchronisation;
    private $users_in_base;

    public function __construct() {
        $this->readConfig();
        $this->initConnection();
        $this->em = new EntityManager($this->conn);
        $this->dm = new DatabaseManager($this->conn);
        $this->synchronisation = new Synchronisation($this->dm);
    }

    public function getConn() {
        return $this->conn;
    }

    public function getEntityManager(): EntityManager {
        return $this->em;
    }

    public function isUsersInBase() {
        return $this->users_in_base;    // renvoie true ou false => synchro securité avec base
    }

    public function migrateDB() {
        $this->synchronisation->migrateBdd();
    }

    private function readConfig() {
        $configuration = Framework::readConfig("orm.ini");
        $this->user = $configuration['db_user'];
        $this->pass = $configuration['db_pass'];
        $this->host = $configuration['db_host'];
        $this->port = $configuration['db_port'];
        $this->dbname = $configuration['db_name'];
        $this->extension = $configuration['db_extension'];

        $this->users_in_base = Framework::readConfig("security.ini")['users_in_db'];
    }

    private function initConnection() {
        $dsn = $this->extension . ':dbname=' . $this->dbname . ';host=' . $this->host;
        $this->conn = new PDO($dsn, $this->user, $this->pass);
    }

}
