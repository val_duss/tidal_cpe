<?php

/*
 *  
 *  Entity Manager de l'ORM du Framework du projet TIDAL
 *
 */

/**
 *
 * @author eloi.cambray-lagassy
 */
class EntityManager {

    private $conn;
    private $toPersist;
    private $todoRequest;

    public function __construct($conn) {
        $this->conn = $conn;
        $this->toPersist = array();
        $this->todoRequest = array();
    }

    public function findAll($entityName) {
        $sql = "SELECT * FROM public.orm_" . $entityName . ";";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $entities = array();
        foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $entityData) {
            $entity = $this->arrayToEntity($entityData, new $entityName());
            if ($entity) {
                $entities[] = $entity;
            }
        }
        return $entities;
    }

    public function find($id, $entityName) {
        $sql = "SELECT * FROM public.orm_" . $entityName . " WHERE id = :id ;";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute([":id" => $id]);
        return $this->arrayToEntity($stmt->fetch(PDO::FETCH_ASSOC), new $entityName());
    }

    public function flush() {

        foreach ($this->toPersist as $persist) {
            $stmt = $this->conn->prepare($persist['req']);
            $stmt->execute($persist['values']);
        }

        foreach ($this->todoRequest as $stmt) {
            $stmt->execute();
        }

        $this->toPersist = array();
        $this->todoRequest = array();
    }

    public function persist($entity) {
        $this->toPersist[] = ['req' => $this->prepareInsertRequest($entity),
            'values' => $this->getValuesToRequest($entity)];
    }

    public function remove($entity) {
        $class = get_class($entity);
        $sql = "DELETE FROM public.orm_" . strtolower($class) . " WHERE id = :id ";
        $stmt = $this->conn->prepare($sql);
        $stmt->bindValue(":id", $entity->getId());
        $this->todoRequest[] = $stmt;
    }

    private function prepareInsertRequest($entity) {
        $class = get_class($entity);
        $sql = "INSERT INTO public.orm_" . strtolower($class) . "(id, ";
        $attr = $this->getAttrList($class);

        foreach ($attr as $key => $attribute) {
            if ($key === sizeof($attr) - 1) {
                $sql .= $attribute . ") ";
            } else {
                $sql .= $attribute . ", ";
            }
            // on ajoute tous les attributs à la requete SQl
        }

        $sql .= " VALUES (nextval('id_orm_" . strtolower($class) . "_sequence'), ";
        foreach ($attr as $key => $attribute) {
            if ($key === sizeof($attr) - 1) {
                $sql .= ":" . $attribute . "); ";
            } else {
                $sql .= ":" . $attribute . ", ";
            }
            // on ajoute tous les attributs à la requete SQl
        }
        return $sql;
    }

    private function getValuesToRequest($entity) {
        $values = array();
        $class = get_class($entity);
        $attr = $this->getAttrList($class);

        foreach ($attr as $attribute) {
            $values[':' . $attribute] = $entity->{"get" . ucfirst($attribute)}();
        }

        return $values;
    }

    private function arrayToEntity($data, $entity) {
        $valRetour = false;
        if ($data) {
            foreach (get_class_methods(get_class($entity)) as $method) {
                if (strpos($method, "get") === 0) {
                    $attr = substr($method, 3);
                    $entity->{"set" . $attr}($data[strtolower($attr)]);
                }
            }
            $valRetour = $entity;
        }

        return $valRetour;
    }

    private function getAttrList($class) {
        $attr = array();

        foreach (get_class_methods($class) as $method) {
            if (strpos($method, "get") === 0) {
                $attribute = strtolower(substr($method, 3));
                if ($attribute !== "id") {
                    $attr[] = $attribute;
                }
            }
        }

        return $attr;
    }

}
