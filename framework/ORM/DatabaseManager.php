<?php

/*
 *  
 *  Database Manager de l'ORM du Framework du projet TIDAL
 * 
 */

/**
 *
 * @author eloi.cambray-lagassy
 */
class DatabaseManager {

    private $conn;
    private $stmt;

    public function __construct(PDO $conn) {
        $this->conn = $conn;
    }

    public function createTable(String $nomTable = "") {
        $valRetour = false;
        $nomTable = 'ORM_' . $nomTable;
        $nomTable = str_replace(";", ",", $nomTable);  // éviter les bugs de base 
        if ($nomTable !== "") {
            if ($this->conn->query("CREATE TABLE IF NOT EXISTS $nomTable ()") === false) {
                print_r($this->conn->errorInfo());
            }
            $valRetour = true;
            $this->createChamp($nomTable, 'id', 'INTEGER', false, true);
        }
        return $valRetour;
    }

    public function dropTable(String $nomTable = "") {
        $valRetour = false;
        $nomTable = 'ORM_' . $nomTable;

        if ($nomTable !== "") {
            $this->stmt = $this->conn->prepare("DROP TABLE ?");
            $this->stmt->execute([$nomTable]);
            $valRetour = true;
        }
        return $valRetour;
    }

    public function createChamp(String $nomTable = "", String $nomChamp = "", String $type = "", $notnull = false, $pk = false) {
        $valRetour = false;

        $nomTable = str_replace(";", ",", $nomTable);  // éviter les bugs de base 
        $nomChamp = str_replace(";", ",", $nomChamp);
        $type = str_replace(";", ",", $type);

        $type === "" ? $type = "VARCHAR(128)" : null;

        if ($nomTable !== "" && $nomChamp !== "" && $type !== "") {
            $req = "ALTER TABLE " . $nomTable . " ADD COLUMN  IF NOT EXISTS $nomChamp $type";
            if ($pk) {
                $req .= " PRIMARY KEY;";
            } else if ($notnull) {
                $req .= " NOT NULL;";
            } else {
                $req .= ";";
            }

            $this->conn->query($req);
            $valRetour = true;

            if ($pk) {
                $this->createSequence("id_" . $nomTable . "_sequence");
            }
        }
        return $valRetour;
    }

    public function createSequence(String $nomSequence = "", $increment = 1) {
        $valRetour = false;
        if ($nomSequence !== "") {
            $this->conn->query("CREATE SEQUENCE IF NOT EXISTS $nomSequence INCREMENT $increment START 1");
            $valRetour = true;
        }
        return $valRetour;
    }

    public function dropSequence(String $nomSequence = "") {
        $valRetour = false;
        if ($nomSequence !== "") {
            $this->stmt = $this->conn->prepare("DROP SEQUENCE ?");
            $this->stmt->execute([$nomSequence]);
            $valRetour = true;
        }
        return $valRetour;
    }

    public function getAllTable() {
        
    }

}
