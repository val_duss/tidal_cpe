<?php

/**
 * Description of user
 *
 * @author eloi.cambray-lagassy
 */
class UserEntity {

    private $id;
    private $username;
    private $password;
    private $name;
    private $firstName;
    private $email;
    private $roles;

    function getId() {
        return $this->id;
    }

    function getUsername() {
        return $this->username;
    }

    function getPassword() {
        return $this->password;
    }

    function getName() {
        return $this->name;
    }

    function getFirstName() {
        return $this->firstName;
    }

    function getEmail() {
        return $this->email;
    }

    function getRoles() {
        return explode(";", $this->getRoles());
    }

    function setId($id): void {
        $this->id = $id;
    }

    function setUsername($username): void {
        $this->username = $username;
    }

    function setPassword($password): void {
        $this->password = $password;
    }

    function setName($name): void {
        $this->name = $name;
    }

    function setFirstName($firstName): void {
        $this->firstName = $firstName;
    }

    function setEmail($email): void {
        $this->email = $email;
    }

    function setRoles($roles): void {
        $this->roles = implode(";", $roles);
    }

}
