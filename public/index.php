<?php

include_once __DIR__ . '/../framework/routing/routing.php';
include_once '../framework/routing/controllers/Controller.php';
include_once "../framework/Authentification/User.php";
include_once '../src/controllers/ControllerRegister.php';
include_once '../src/controllers/ControllerMember.php';
include_once '../src/controllers/ControllerLogin.php';
include_once '../src/controllers/ControllerArticles.php';


/** Création de base pour l'auth **/
$session = new Session();
$users = new Users($session);
$Member = new Role("Member");
$Member->addRulesOnePage("home");//Ajout d'une page whitelist pour un role

$users->newAttribute("int", "age");
$users->newAttribute("string", "prenom"); 


$users->register("Valentin", "1234",["11","Valentin"]); //Création d'un nouvel utilisateur
$users->register("Eloi", "1234",["2","Eloi"]); //Création d'un nouvel utilisateur
$users->getUserById(0)->addRole($Member); //Ajout d'un role à un utilisateur


/** Création du Routing **/
$routing = new Routing();
$register = new ControllerRegister("register", $users);
$routing->addRoute($register);
$routing->addRoute(new ControllerLogin("login",$users));
$routing->addRoute(new ControllerMember("home",$users));
$routing->addRoute(new ControllerArticles("articles", $users));
$routing->routing();