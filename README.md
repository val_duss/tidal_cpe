# ProjetTIDAL

##Routing

La class routing stocke les controlleurs dans le tableau *routes* de la class *Routing*

    $routing = new Routing();
    $routing->addRoute(new Controller("bla"));
    $routing->routing();

Chaque controlleur possède un nom, une vue et un model. Les controlleurs fils, les models et les vues sont stocké dans le dossier `/src`

Ainsi la class controller peut être surchargé suivant les attentes de l'utilisateur

##Authentification

###Sessions
La class Session permet de créer des sessions suivant un user_id, de vérifier si l'utilisateur est connecté et donner l'id de l'utilisateur connecté

    $session = new Session(23); //Outils de création des sessions
    if($session->isConnected()){ //Vérifie si l'utilisateur est connecté
        echo $session->getConnectedUserID(); //Récupère l'ID de l'utilisateur
    }
    $session->stopSession(23) //Stop la session

###Users
L'ensemble des utilisateurs est stocké dans la class *Users* .
La class *Users* permet de gérer les attributs optionnels des utilisateurs, une liste d'attributs communs au utilisateur. Les getter et setter de cette liste sont présent

    $users->newAttribute("int", "age"); //Nouveau attribut
    $users->newAttribute("string", "prenom");
    $users->deleteAttribute($users->getAttributesList()[0]);//Suppression D'attribut

La class *Users* gère aussi la connection et l'inscription de nouveaux utilisateurs

    $users->register("Valentin", "1234",["11","Valentin"]); //Création d'un nouvel utilisateur
    $users->login("Valentin", "1234"); //Connection d'un utilisateur

###Rôle
La class Rôle possède un nom et une liste de page whitelist pour les utilisateurs possédant le rôle en question. Ainsi on peut ajouter et supprimer des pages à l'utilisateur.

    $Member = new Role("Member"); //Exemple de création de rôle
    $Member->addRulesOnePage("pagetest");//Ajout d'une page whitelist pour un role

###User
Chaque *User* possède un liste de rôle

    $users->getUserById($session->getConnectedUserID())->addRole($Member); //Ajout d'un role à un utilisateur  

##FormBuilder

La class *Formbuilder* possède un nom, une url, une methode et une liste d'input. 
Les class Fille de la class *Input* comportent les spécificité de chaque type d'input TextField, Date, File,..

    include '../framework/Formbuilder/formbuilder.php';//Apppel de la class FormBuilder
    $form = new FormBuilder("test");//Création du Formulaire
    $form->setMethodeGet();//Définition de la methode GET
    $form->addNewTextField("test","testId","Veuillez entrer une valeur de test");//Ajout de l'input TextField
    $form->affiche();//Affichage du formulaire

##ORM

L'ORM permet d'administrer toute la base de données en simple POO via un systèmes d'entités.
Pour l'utiliser il suffit d'include les classes entitié créée dans le dossier src/entities. Pour que l'ORM puisse
fonctionner il est nécessaire de définir les Getter et Setter de chaque attribut de l'entité. Sachant que le champ
ID est obligatoire.

L'ORM gère aujourd'hui l'insertion le remove et le find (Findall). Pour qu'une entité soit sauvegardée en base il
faut tout d'abord l'informer à l'entitymanager.

$orm = new ORM();
$em = $orm->getEntityManager();
$em->persist($entite);
$em->flush();

Une fois cela fait il est possible de la récupérer de 2 manière différente pour l'afficher : 
$em->findAll("classe_de_l_entite");  // renvoi la liste d'entite enregistre dans la base
ou 
$em->find("classe_de_l_entite", $id); // renvoie que l'entite voulue

Une fois les entités récupérées on peut y accéder en simple POO via les getter sur les attributs, par exemple :
$article->getTitre();       // renvoie le champ titre de article enregistré en base

Pour tester l'ORM 2 commandes sont définies dans le framework, pour les utiliser, se placer à la racine du projet.

php framework/Console.php orm:synchronisation      
// synchronisera la bdd definie dans config/orm.ini avec les entites

php framework/Console.php orm:import
// ajoutera 2 articles pour le test du framework

##Objectif du Framework
Structure de base : page d'accueil du framework tant qu'aucune route n'existe

Fonctionnalités du framework PHP
* Routing
    => redirige automatiquement sur la bonne action des controleurs en fonction de la route
    
* Gère l'authentification en natif
    => gestion transparente des sessions pour l'utilisateur du framework
    => gestion des roles et possibilités de limiter l'accès à certains controleurs
    
* Gère les formulaires
    => génere la vue en HTML
    => gère le submit et actualise la bdd directement
    
* ORM type Doctrine
     => Lecture des données
     => Ecriture des données (modification / ajout)

Voir si on intègre Twig pour le templating


mdp vm -> tidal/1234567890
