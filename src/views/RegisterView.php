<!DOCTYPE html>
<html>
<head>
	<title><?= $title ?></title>
</head>
<body>
	<?= $content ?>
	<?php
		include '../framework/Formbuilder/formbuilder.php'; 
		$form = new FormBuilder("register");
		$form->setMethodePost();
		$form->addNewTextField("name","name","Veuillez entrer votre login");
		$form->addNewPassword("password","password","Veuillez entrer votre mot de passe");
		$form->addNewTextField("age","age","Veuillez entrer votre âge");
		$form->addNewTextField("prenom","prenom","Veuillez entrer votre prénom");
		$form->affiche();
	?>
</body>
</html>