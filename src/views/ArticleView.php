<!DOCTYPE html>
<html>
    <head>
        <title><?= $title ?></title>
    </head>
    <body>
        <?= $content ?>
        <section>
            <?php foreach ($articles as $article) { ?>
                <article>
                    <h2>
                        <?= $article->getTitre() ?>
                    </h2>
                    <p>
                        <?= $article->getCorps() ?>
                    </p>
                    <br/>
                    <em><?= $article->getPied() ?></em>
                </article>
                <?php
            }
            ?>
        </section>
    </body>
</html>