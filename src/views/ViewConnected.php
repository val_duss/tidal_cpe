<!DOCTYPE html>
<html>
<head>
	<title><?= $title ?></title>
</head>
<body>
	<?= $content ?>
	<?php
		include '../framework/Formbuilder/formbuilder.php'; 
		$form = new FormBuilder("logout");
		$form->setMethodePost();
		$form->addNewHidden("logout","logout");
		$form->setSubmit("logout");
		$form->affiche();
	?>
</body>
</html>