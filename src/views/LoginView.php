<!DOCTYPE html>
<html>
<head>
	<title><?= $title ?></title>
</head>
<body>
	<?= $content ?>
	<?php
		include '../framework/Formbuilder/formbuilder.php'; 
		$form = new FormBuilder("login");
		$form->setMethodePost();
		$form->addNewTextField("name","name","Veuillez entrer votre login");
		$form->addNewPassword("password","password","Veuillez entrer votre mot de passe");
		$form->affiche();
	?>
</body>
</html>