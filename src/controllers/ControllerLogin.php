<?php
include_once '../framework/routing/controllers/Controller.php';

class ControllerLogin extends Controller{

	private $users;

	function __construct($name="login", $users){
        $this->model = "LoginModel";
        $this->view = "LoginView";
        $this->name = $name;
        $this->users = $users;
    }

    private function login(){
        if(isset($_POST['name']) && isset($_POST['password'])){
           if($_POST['name'] != NULL && $_POST['password'] != NULL){
                if($this->users->login($_POST['name'], $_POST['password'])){
                    $this->goTo("/tidal_cpe/public/home");
                }else{
                    echo "Erreur";
                }
            }
        }        
    }

    function affiche(){
        $this->login();
        require('../src/models/'.$this->model.'.php');
        $title = $this->name;
        $content = getContent();
        require('../src/views/'.$this->view.'.php');
    }


}
