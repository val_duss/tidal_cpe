<?php

include_once '../framework/routing/controllers/Controller.php';
include_once '../framework/ORM/ORM.php';
include_once __DIR__ . '/../entities/Article.php';

class ControllerArticles extends Controller {

    private $entityManager;

    function __construct($name = "articles", $users) {
        $this->model = "ArticleModel";
        $this->view = "ArticleView";
        $this->name = $name;
        $this->entityManager = (new ORM())->getEntityManager();
    }


    function affiche() {
        require(__DIR__ . '/../models/' . $this->model . '.php');
        $title = $this->name;
        $articles = $this->entityManager->findAll("article");
        $content = getContent();
        require(__DIR__ . '/../views/' . $this->view . '.php');
    }

}
