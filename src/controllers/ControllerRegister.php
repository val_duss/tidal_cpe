<?php
include_once '../framework/routing/controllers/Controller.php';

class ControllerRegister extends Controller{

	private $users;

	function __construct($name="login", $users){
        $this->model = "Model";
        $this->view = "RegisterView";
        $this->name = $name;
        $this->users = $users;
    }

    private function register(){
        if(isset($_POST['name']) && isset($_POST['password'])){
           if($_POST['name'] != NULL && $_POST['password'] != NULL){
            $name = htmlspecialchars(trim($_POST['name']));
            $password = htmlspecialchars(trim($_POST['password']));
            $age = htmlspecialchars(trim($_POST['age']));
            $prenom = htmlspecialchars(trim($_POST['prenom']));

            $this->users->register($name, $password,[$age,$prenom]);
            $this->goTo("/tidal_cpe/public/home");
            }
        }        
    }

    function affiche(){
        $this->register();
    	require('../src/models/'.$this->model.'.php');
        $title = $this->name;
        $content = getContent();
        require('../src/views/'.$this->view.'.php');
    }


}
