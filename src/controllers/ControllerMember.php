<?php
include_once '../framework/routing/controllers/Controller.php';

class ControllerMember extends Controller{

	private $users;

	function __construct($name="home", $users){
        $this->model = "ModelMemberExample";
        $this->view = "View";
        $this->name = $name;
        $this->users = $users;
    }

    private function getPost(){
        if(isset($_POST['logout'])){
            $this->users->getSession()->stopSession();
        }
    }


    function affiche(){
    	require('../src/models/'.$this->model.'.php');
        $title = $this->name;
        $this->getPost();
        if($this->users->getSession()->isConnected()){
            $content = getContentConnected();
            if($this->users->getUserById($this->users->getSession()->getConnectedUserID())->isAuthorized("home")){
                $content .= "<br><br> Vous avez le rôle membre et donc vous êtes autorisé à voir cette partie";
            }else{
                $content .= "<br><br> Vous n'avez pas le rôle membre et donc vous n'êtes pas autorisé à voir cette partie";
            }
            require('../src/views/ViewConnected.php');
        }else{
            $content = getContent();
            require('../src/views/'.$this->view.'.php');
        }
    }


}
