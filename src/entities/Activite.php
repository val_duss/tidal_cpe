<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Activite
 *
 * @author eloi.cambray-lagassy
 */
class Activite {

    private $id;
    private $duree;
    private $nom;

    function getId() {
        return $this->id;
    }

    function getDuree() {
        return $this->duree;
    }

    function getNom() {
        return $this->nom;
    }

    function setId($id): void {
        $this->id = $id;
    }

    function setDuree($duree): void {
        $this->duree = $duree;
    }

    function setNom($nom): void {
        $this->nom = $nom;
    }

}
