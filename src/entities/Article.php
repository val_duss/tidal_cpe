<?php

/**
 * Description of Article
 *
 * @author eloi.cambray-lagassy
 */
class Article {

    private $id;
    private $titre;
    private $corps;
    private $pied;
    private $ordre;

    function getId() {
        return $this->id;
    }

    function getTitre() {
        return $this->titre;
    }

    function getCorps() {
        return $this->corps;
    }

    function getPied() {
        return $this->pied;
    }

    function getOrdre() {
        return $this->ordre;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setTitre($titre): void {
        $this->titre = $titre;
    }

    function setCorps($corps): void {
        $this->corps = $corps;
    }

    function setPied($pied): void {
        $this->pied = $pied;
    }

    function setOrdre($ordre): void {
        $this->ordre = $ordre;
    }

}
